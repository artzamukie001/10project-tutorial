window.onload = function() {
    var secound = 00
    var tens = 00
    var appendTens = document.getElementById("tens")
    var appendSecound = document.getElementById("secound")
    var buttonStart = document.getElementById("button-start")
    var buttonStop = document.getElementById("button-stop")
    var buttonReset = document.getElementById("button-reset")
    var Interval

    buttonStart.onclick = function() {
        clearInterval(Interval)
        Interval = setInterval(startTimer,10)
    }

    buttonStop.onclick = function() {
        clearInterval(Interval)
    }

    buttonReset.onclick = function() {
        clearInterval(Interval)
        tens = "00"
        secound = "00"
        appendTens.innerHTML = tens
        appendSecound.innerHTML = secound
    }

    function startTimer() {
        tens ++

        if(tens < 9) {
            appendTens.innerHTML = "0" + tens
        }
        if(tens > 9) {
            appendTens.innerHTML = tens
        }
        if(tens > 99) {
            secound ++
            appendSecound.innerHTML = "0" + secound
            tens = 0
            appendTens.innerHTML = "0" + 0
        }
        if(secound > 9) {
            appendSecound.innerHTML = secound
        }
    }
}