var hourHand = document.querySelector('.hour-hand')
var minuteHand = document.querySelector('.minute-hand')
var secoundHand = document.querySelector('.secound-hand')

function setDate() {
    var now = new Date()

    var hours = now.getHours()
    var hoursDegree = Math.round(((hours / 12) * 360) +90)

    var minutes = now.getMinutes()
    var minutesDegree = Math.round(((minutes / 60) * 360) +90)

    var secounds = now.getSeconds()
    var secoundsDegree = Math.round(((secounds / 60) * 360) +90)

    hourHand.style.transform = `rotate(${hoursDegree}deg)`
    secoundHand.style.transform = `rotate(${secoundsDegree}deg)`
    minuteHand.style.transform = `rotate(${minutesDegree}deg)`

    console.log(hours + ":" + minutes + ":" + secounds);
}

setInterval(setDate, 1000)