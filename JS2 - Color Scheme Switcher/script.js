document.getElementById("greyButton").onclick = switchGrey
document.getElementById("whiteButton").onclick = switchWhite
document.getElementById("blueButton").onclick = switchBlue
document.getElementById("yellowButton").onclick = switchYellow

function switchGrey() {
    document.getElementsByTagName("body")[0].style.backgroundColor = 'gainsboro'
    document.getElementsByTagName("body")[0].style.color = 'snow'
}

function switchWhite() {
    document.getElementsByTagName("body")[0].style.backgroundColor = 'snow'
    document.getElementsByTagName("body")[0].style.color = 'black'
}

function switchBlue() {
    document.getElementsByTagName("body")[0].style.backgroundColor = 'cornflowerblue'
    document.getElementsByTagName("body")[0].style.color = 'snow'
}

function switchYellow() {
    document.getElementsByTagName("body")[0].style.backgroundColor = 'tan'
    document.getElementsByTagName("body")[0].style.color = 'black'
}
